package com.example.dataaccess.exceptions;

import lombok.Data;

@Data
public class ApplicationException extends Exception {

    private ApiError apiError;

    public ApplicationException(ApiError apiError){
        super();
        this.apiError = apiError;
    }
}
