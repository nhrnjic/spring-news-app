package com.example.dataaccess.exceptions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

@Data
public class ApiError {

    @JsonIgnore
    private HttpStatus httpStatus;
    private List<String> errorMessages;

    public ApiError(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.errorMessages = Arrays.asList(message);
    }

    public ApiError(HttpStatus httpStatus, List<String> errorMessages) {
        this.httpStatus = httpStatus;
        this.errorMessages = errorMessages;
    }
}

