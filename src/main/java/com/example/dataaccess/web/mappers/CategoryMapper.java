package com.example.dataaccess.web.mappers;

import com.example.dataaccess.db.models.DBCategory;
import com.example.dataaccess.web.dto.CategoryDTO;

public class CategoryMapper {

    public static DBCategory toDb(CategoryDTO categoryDTO){
        DBCategory category = new DBCategory();
        category.setId(categoryDTO.getId());
        category.setName(categoryDTO.getName());
        category.setOrderNumber(categoryDTO.getOrderNumber());
        category.setShowInNavigation(categoryDTO.isShowInNavigation());
        return category;
    }

    public static CategoryDTO fromDb(DBCategory dbCategory){
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(dbCategory.getId());
        categoryDTO.setName(dbCategory.getName());
        categoryDTO.setOrderNumber(dbCategory.getOrderNumber());
        categoryDTO.setShowInNavigation(dbCategory.isShowInNavigation());
        return categoryDTO;
    }
}
