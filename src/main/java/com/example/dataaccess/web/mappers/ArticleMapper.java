package com.example.dataaccess.web.mappers;

import com.example.dataaccess.db.models.DBArticle;
import com.example.dataaccess.web.dto.ArticleDTO;

public class ArticleMapper {

    public static ArticleDTO fromDb(DBArticle dbArticle){
        ArticleDTO articleDTO = new ArticleDTO();
        articleDTO.setId(dbArticle.getId());
        articleDTO.setAuthorName(dbArticle.getAuthorName());
        articleDTO.setActive(dbArticle.isActive());
        articleDTO.setContent(dbArticle.getContent());
        articleDTO.setCoverFigure(dbArticle.getCoverFigure());
        articleDTO.setCoverImg(dbArticle.getCoverImg());
        articleDTO.setHeadingObject(dbArticle.getHeadingObject());
        articleDTO.setName(dbArticle.getName());
        articleDTO.setCreatedAt(dbArticle.getCreatedAt());
        articleDTO.setDescription(dbArticle.getDescription());
        articleDTO.setShortName(dbArticle.getShortName());
        articleDTO.setThumbnail(dbArticle.getThumbnail());
        articleDTO.setVideolink(dbArticle.getVideolink());
        articleDTO.setGalleryImages(dbArticle.splitGalleryImages());
        return articleDTO;
    }

    public static DBArticle toDb(ArticleDTO articleDTO){
        DBArticle article = new DBArticle();
        article.setId(articleDTO.getId());
        article.setAuthorName(articleDTO.getAuthorName());
        article.setActive(articleDTO.isActive());
        article.setContent(articleDTO.getContent());
        article.setCoverFigure(articleDTO.getCoverFigure());
        article.setCoverImg(articleDTO.getCoverImg());
        article.setHeadingObject(articleDTO.getHeadingObject());
        article.setName(articleDTO.getName());
        article.setCreatedAt(articleDTO.getCreatedAt());
        article.setDescription(articleDTO.getDescription());
        article.setShortName(articleDTO.getShortName());
        article.setThumbnail(articleDTO.getThumbnail());
        article.setVideolink(articleDTO.getVideolink());

        if(articleDTO.getGalleryImages() != null){
            article.setGalleryImages(String.join(",", articleDTO.getGalleryImages()));
        }

        return article;
    }
}
