package com.example.dataaccess.web.controllers;

import com.example.dataaccess.db.models.DBCategory;
import com.example.dataaccess.db.projections.CategoryArticlesDTO;
import com.example.dataaccess.db.repositories.CategoryRepository;
import com.example.dataaccess.exceptions.ApiError;
import com.example.dataaccess.exceptions.ApplicationException;
import com.example.dataaccess.web.dto.CategoryDTO;
import com.example.dataaccess.web.mappers.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/categories")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllCategories(){
        List<CategoryDTO> categories = categoryRepository.getAllCategories();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<CategoryDTO> getCategory(@PathVariable(name = "id") Long id) throws ApplicationException{
        CategoryDTO category = categoryRepository.getById(id)
                .orElseThrow(() -> {
                    ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Category not found");
                    return new ApplicationException(apiError);
                });

        return new ResponseEntity<>(category ,HttpStatus.OK);
    }

    @RequestMapping(value = "/articles", method = RequestMethod.GET)
    public List<CategoryArticlesDTO> getCategoryArticles(){
        return categoryRepository.getCategoryArticles();
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<CategoryDTO> createArticle(@Valid @RequestBody DBCategory category){
        return new ResponseEntity<>(CategoryMapper.fromDb(categoryRepository.save(category)), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ResponseEntity<CategoryDTO> updateCategory(@PathVariable(name = "id") Long id,
                                                     @Valid @RequestBody DBCategory category) throws ApplicationException{
        if(!categoryRepository.existsById(id)){
            ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Category not found");
            throw new ApplicationException(apiError);
        }else{
            return new ResponseEntity<>(CategoryMapper.fromDb(categoryRepository.save(category)), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteCategory(@PathVariable(name = "id") Long id) throws ApplicationException{
        if(!categoryRepository.existsById(id)){
            ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Category not found");
            throw new ApplicationException(apiError);
        }else{
            categoryRepository.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        }
    }
}
