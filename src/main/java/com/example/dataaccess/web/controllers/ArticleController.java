package com.example.dataaccess.web.controllers;

import com.example.dataaccess.db.models.DBArticle;
import com.example.dataaccess.db.repositories.ArticleRepository;
import com.example.dataaccess.exceptions.ApiError;
import com.example.dataaccess.exceptions.ApplicationException;
import com.example.dataaccess.web.dto.ArticleDTO;
import com.example.dataaccess.web.mappers.ArticleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping(value = "/articles")
public class ArticleController {

    @Autowired
    private ArticleRepository articleRepository;
    
    @RequestMapping(method = RequestMethod.GET)
    public List<ArticleDTO> getAllArticles(){
        List<ArticleDTO> articles = StreamSupport.stream(articleRepository.findAll().spliterator(), false)
                .map(ArticleMapper::fromDb)
                .collect(Collectors.toList());

        return articles;
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ResponseEntity<ArticleDTO> getArticle(@PathVariable(name = "id") Long id) throws ApplicationException{
        DBArticle article = articleRepository.findById(id)
                .orElseThrow(() -> {
                    ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Article not found");
                    return new ApplicationException(apiError);
                });

        return new ResponseEntity<>(ArticleMapper.fromDb(article), HttpStatus.OK);
    }

    @RequestMapping(value = "/short_name/{shortName}", method = RequestMethod.GET)
    public ResponseEntity<ArticleDTO> getArticleByShortname(@PathVariable(name = "shortName") String shortName) throws ApplicationException{
        DBArticle article = articleRepository.findByShortName(shortName)
                .orElseThrow(() -> {
                    ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Article not found");
                    return new ApplicationException(apiError);
                });

        return new ResponseEntity<>(ArticleMapper.fromDb(article), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ArticleDTO> createArticle(@Valid @RequestBody ArticleDTO articleDTO){
        DBArticle article = ArticleMapper.toDb(articleDTO);
        articleRepository.save(article);
        return new ResponseEntity(ArticleMapper.fromDb(article) ,HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateArticle(@PathVariable(name = "id") Long id,
                                           @Valid @RequestBody ArticleDTO articleDTO) throws ApplicationException{
        DBArticle article = articleRepository.findById(id)
                .orElseThrow(() -> {
                    ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Article not found");
                    return new ApplicationException(apiError);
                });

        DBArticle articleForUpdate = ArticleMapper.toDb(articleDTO);
        articleRepository.save(articleForUpdate);
        return new ResponseEntity<>(articleForUpdate, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteArticle(@PathVariable(name = "id") Long id) throws ApplicationException{
        if(!articleRepository.existsById(id)){
            ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Article not found");
            throw new ApplicationException(apiError);
        }else{
            articleRepository.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        }
    }

}











