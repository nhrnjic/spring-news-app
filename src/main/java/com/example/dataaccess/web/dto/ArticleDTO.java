package com.example.dataaccess.web.dto;

import com.example.dataaccess.db.models.DBUser;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
public class ArticleDTO {
    private Long id;

    @NotNull(message = "Name field is required.")
    @NotBlank(message = "Name field can't be blank.")
    private String name;

    @NotNull(message = "Article content is required.")
    @NotBlank(message = "Article content can't be blank.")
    private String content;

    private String videolink;

    @NotNull(message = "Article active status is required.")
    private boolean isActive;

    @NotNull(message = "Article short name is required.")
    @NotBlank(message = "Article short name can't be blank.")
    private String shortName;

    @NotNull(message = "Article cover image is required.")
    @NotBlank(message = "Article cover image can't be blank.")
    private String coverImg;

    private List<String> galleryImages;

    @NotNull(message = "Article thumbnail image is required.")
    @NotBlank(message = "Article thumbnail image can't be blank.")
    private String thumbnail;

    @NotNull(message = "Article creation date is required.")
    private Date createdAt;

    @NotNull(message = "Article description is required.")
    @NotBlank(message = "Article description can't be blank.")
    private String description;

    private String authorName;

    @NotNull(message = "Article heading object is required.")
    @NotBlank(message = "Article heading object can't be blank.")
    private String headingObject;

    private String coverFigure;
}
