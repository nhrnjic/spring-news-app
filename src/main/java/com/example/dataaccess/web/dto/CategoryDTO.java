package com.example.dataaccess.web.dto;

import com.example.dataaccess.db.models.DBArticle;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class CategoryDTO {

    private Long id;

    @NotNull(message = "Category name is required.")
    @NotBlank(message = "Category name can't be blank.")
    private String name;

    private int orderNumber;

    private boolean showInNavigation;

    public CategoryDTO(){}

    public CategoryDTO(Long id, String name, int orderNumber, boolean showInNavigation) {
        this.id = id;
        this.name = name;
        this.orderNumber = orderNumber;
        this.showInNavigation = showInNavigation;
    }
}
