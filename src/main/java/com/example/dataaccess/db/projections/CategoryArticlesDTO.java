package com.example.dataaccess.db.projections;

import java.util.Date;

public interface CategoryArticlesDTO{
    Long getId();

    String getName();

    String getContent();

    String getVideolink();

    boolean isActive();

    String getShortname();

    String getCoverimg();

    String getGalleryimages();

    String getThumbnail();

    Date getCreatedat();

    String getDescription();

    String getAuthor_name();

    String getHeading_object();

    String getCover_figure();

    Long getUserid();

    Long getCategory_id();

    String getCategory_name();

    String getShow_in_nav();
}
