package com.example.dataaccess.db.repositories;

import com.example.dataaccess.db.models.DBArticle;
import com.example.dataaccess.web.dto.ArticleDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ArticleRepository extends CrudRepository<DBArticle, Long> {

    Optional<DBArticle> findByShortName(String shortName);
}
