package com.example.dataaccess.db.repositories;

import com.example.dataaccess.db.models.DBCategory;
import com.example.dataaccess.db.projections.CategoryArticlesDTO;
import com.example.dataaccess.web.dto.CategoryDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends CrudRepository<DBCategory, Long> {
    @Query("select new com.example.dataaccess.web.dto.CategoryDTO(c.id, c.name, c.orderNumber, c.showInNavigation) " +
            "from DBCategory c")
    List<CategoryDTO> getAllCategories();

    @Query("select new com.example.dataaccess.web.dto.CategoryDTO(c.id, c.name, c.orderNumber, c.showInNavigation) " +
            "from DBCategory c " +
            "where c.id = ?1")
    Optional<CategoryDTO> getById(Long id);

    @Query(value = " select a.*, c.id as category_id, c.name as category_name, c.show_in_navigation as show_in_nav from article as a\n" +
            "        inner join article_category as ac\n" +
            "        on a.id = ac.article_id\n" +
            "        inner join category as c\n" +
            "        on c.id = ac.category_id\n" +
            "        where a.active = true\n" +
            "        order by c.order_number", nativeQuery = true)
    List<CategoryArticlesDTO> getCategoryArticles();
}


