package com.example.dataaccess.db.repositories;

import com.example.dataaccess.db.models.DBUser;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<DBUser, Long> {
}
