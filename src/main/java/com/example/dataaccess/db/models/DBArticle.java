package com.example.dataaccess.db.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "article")
@Getter
@Setter
public class DBArticle {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    @NotBlank
    @Column(name = "name")
    private String name;

    @NotNull
    @NotBlank
    @Column(name = "content")
    private String content;

    @Column(name = "videolink")
    private String videolink;

    @NotNull
    @Column(name = "active")
    private boolean isActive;

    @NotNull
    @NotBlank
    @Column(name = "shortname")
    private String shortName;

    @NotNull
    @NotBlank
    @Column(name = "coverimg")
    private String coverImg;

    @Column(name = "galleryimages")
    private String galleryImages;

    @NotNull
    @NotBlank
    @Column(name = "thumbnail")
    private String thumbnail;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "userid")
    private DBUser user;

    @NotNull
    @Column(name = "createdat")
    private Date createdAt;

    @NotNull
    @NotBlank
    @Column(name = "description")
    private String description;

    @Column(name = "author_name")
    private String authorName;

    @NotNull
    @NotBlank
    @Column(name = "heading_object")
    private String headingObject;

    @Column(name = "cover_figure")
    private String coverFigure;

    public List<String> splitGalleryImages(){
        if(galleryImages == null || galleryImages.equals("")){
            return null;
        }else{
            return Arrays.asList(galleryImages.split(", "));
        }
    }
}
