package com.example.dataaccess.db.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "category")
@Data
public class DBCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull(message = "Category name is required.")
    @NotBlank(message = "Category name can't be blank.")
    @Column(name = "name")
    private String name;

    @Column(name = "order_number")
    private int orderNumber;

    @Column(name = "show_in_navigation")
    private boolean showInNavigation;

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable(
            name = "article_category",
            joinColumns = @JoinColumn(name = "category_id"),
            inverseJoinColumns = @JoinColumn(name = "article_id")
    )
    private List<DBArticle> articles = new ArrayList<>();

    public DBCategory(){

    }

    public DBCategory(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
