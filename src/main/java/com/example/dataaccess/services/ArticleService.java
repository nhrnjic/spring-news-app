package com.example.dataaccess.services;

import com.example.dataaccess.db.models.DBArticle;
import com.example.dataaccess.db.repositories.ArticleRepository;
import com.example.dataaccess.exceptions.ApiError;
import com.example.dataaccess.exceptions.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    public DBArticle get(Long id) throws ApplicationException{
        DBArticle article = articleRepository.findById(id)
                .orElseThrow(() -> {
                    ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Article not found");
                    return new ApplicationException(apiError);
                });

        return article;
    }
}
